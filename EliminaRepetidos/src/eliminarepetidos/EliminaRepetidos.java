/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eliminarepetidos;

import java.util.Scanner;

/**
 *
 * @author marlo
 */
public class EliminaRepetidos {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner sc = new Scanner(System.in);
        int tamanhoVetor = 5;
        Integer[] vetor = new Integer[tamanhoVetor];
        
        try{
            // Entrada de dados
            for(int i = 0; i < tamanhoVetor; i++){
            System.out.println("Informe o valor do vetor na posição " + i);
            vetor[i] = sc.nextInt();
        }
        
        // Processamento
        for(int i = 0; i < tamanhoVetor; i++){ //  Ordenação do vetor

            for(int v = 0; v < tamanhoVetor; v++ ){
                
                if( v > 0 && vetor[v] < vetor[v-1]){
                    int aux = vetor[v-1];
                    vetor[v-1] = vetor[v];
                    vetor[v] = aux;
                }
            }
        }
        
            System.out.println();
            System.out.println("Resultado Ordenado Sem Repetições:");
            for(int i = 0; i < tamanhoVetor; i++){
                if( (i == 0) || (vetor[i] != vetor[i-1])){
                    System.out.println(vetor[i]);
                }
            }            
            
        }catch(Exception ex){
            System.out.println(ex.getMessage());
        }
    }
    
}
