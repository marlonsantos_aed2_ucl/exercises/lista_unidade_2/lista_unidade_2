/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package numerocomplexo;

import java.util.Scanner;

/**
 *
 * @author Deive
 */
public class NumeroComplexo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        Scanner teclado = new Scanner(System.in);
        
        // https://www.somatematica.com.br/emedio/complexos/complexos.php
        // https://www.infoescola.com/matematica/numeros-complexos/
        // https://sciprolab2.readthedocs.io/en/latest/oop.html <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
        // https://www.youtube.com/watch?v=6k2dtRxVawg& <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
        
        /* // TESTES
        NumeroComplexoTad n = new NumeroComplexoTad(8, -2);
        NumeroComplexoTad m = new NumeroComplexoTad(4, -2);
        System.out.println(n.Soma(m));
        System.out.println(n.Subtracao(m));
        System.out.println(n.Multiplicacao(m));
        System.out.println(n.Divisao(m));
        */
        
        short escolha = (short) 5;
        NumeroComplexoTad primeiroNumeroComplexo = null;
        NumeroComplexoTad segundoNumeroComplexo = null;
        
        while (escolha != 6) {
            
            if (escolha == 5) {
                // CRIANDO O PRIMEIRO NÚMERO COMPLEXO
                System.out.print("Digite o valor real de A para o primeiro número complexo: ");
                double pri_num_real_a = teclado.nextInt();
                System.out.print("Digite o valor real de B para o primeiro número complexo: ");
                double pri_num_real_b = teclado.nextInt();

                primeiroNumeroComplexo = new NumeroComplexoTad(pri_num_real_a, pri_num_real_b);

                // CRIANDO O SEGUNDO NÚMERO COMPLEXO
                System.out.print("Digite o valor real de A para o segundo número complexo: ");
                double seg_num_real_a = teclado.nextInt();
                System.out.print("Digite o valor real de B para o segundo número complexo: ");
                double seg_num_real_b = teclado.nextInt();

                segundoNumeroComplexo = new NumeroComplexoTad(seg_num_real_a, seg_num_real_b);

            }

            // FAZ UMA OPERAÇÃO DE ACORDO COM A ESCOLHA DO USUÁRIO
            System.out.println("1 - Somar o primeiro com o segundo");
            System.out.println("2 - Subtrair o primeiro pelo segundo");
            System.out.println("3 - Multiplicar o primeiro pelo segundo");
            System.out.println("4 - Dividir o primeiro pelo segundo");
            System.out.println("5 - Reconfigurar primeiro e segundo números complexos");
            System.out.println("6 - Sair");

            System.out.print("Escolha uma das opções acima: ");
            escolha = teclado.nextShort();

            switch (escolha) {
                case 1:
                    System.out.println("\n==============\n" + primeiroNumeroComplexo.Soma(segundoNumeroComplexo) + "\n==============\n");
                    break;
                case 2:
                    System.out.println("\n==============\n" + primeiroNumeroComplexo.Subtracao(segundoNumeroComplexo) + "\n==============\n");
                    break;
                case 3:
                    System.out.println("\n==============\n" + primeiroNumeroComplexo.Multiplicacao(segundoNumeroComplexo) + "\n==============\n");
                    break;
                case 4:
                    System.out.println("\n==============\n" + primeiroNumeroComplexo.Divisao(segundoNumeroComplexo) + "\n==============\n");
                    break;
                case 5:
                    escolha = 5;
                    break;
                case 6:
                    System.out.println("\n==============\nSaindo...\n==============\n");
                    escolha = 6;
                    break;
                default:
                    System.out.println("\n==============\nEscolha inválida...\n==============\n");
            }
            
        }
        
        teclado.close();
        
    }
    
}
