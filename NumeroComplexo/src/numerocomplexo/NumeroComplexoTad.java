/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package numerocomplexo;

/**
 *
 * @author Deive
 */
public class NumeroComplexoTad {
    
    private double num_real_a;
    private double num_real_b;
    private String operacao;
    
    public NumeroComplexoTad(double real_a, double real_b) {
        this.num_real_a = real_a;
        this.num_real_b = real_b;
    }
    
    public double GetNumRealA() {
        return this.num_real_a;
    }
    
    public double GetNumRealB() {
        return this.num_real_b;
    }
    
    public String Soma(NumeroComplexoTad n) {
        double real_a = this.num_real_a + n.GetNumRealA();
        double real_b = this.num_real_b + n.GetNumRealB();
        
        return "S = {" + real_a + ", " + real_b + "i" + "}";
    }
    
    public String Subtracao(NumeroComplexoTad n) {
        double real_a = this.num_real_a - n.GetNumRealA();
        double real_b = this.num_real_b - n.GetNumRealB();
        
        return "S = {" + real_a + ", " + real_b + "i" + "}";
    }
    
    public String Multiplicacao(NumeroComplexoTad n) {
        double real_a = this.num_real_a * n.GetNumRealA() - this.num_real_b * n.GetNumRealB();
        double real_b = this.num_real_a * n.GetNumRealB() + this.num_real_b * n.GetNumRealA();
        
        return "S = {" + real_a + ", " + real_b + "i" + "}";
    }
    
    public String Divisao(NumeroComplexoTad n) {
        double real_a = (this.num_real_a * n.GetNumRealA() - this.num_real_b * (n.GetNumRealB() * (-1))) / (n.GetNumRealA() * n.GetNumRealA() - n.GetNumRealB() * (n.GetNumRealB() * (-1)));
        double real_b = (this.num_real_a * (n.GetNumRealB() * (-1)) + (this.num_real_b * n.GetNumRealA())) / (n.GetNumRealA() * n.GetNumRealA() - n.GetNumRealB() * (n.GetNumRealB() * (-1)));
        
        return "S = {" + real_a + ", " + real_b + "i" + "}";
    }
    
}
