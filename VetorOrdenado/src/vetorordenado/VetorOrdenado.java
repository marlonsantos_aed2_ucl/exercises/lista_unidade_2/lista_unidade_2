/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vetorordenado;

import java.lang.reflect.Array;
import java.util.Scanner;

/**
 *
 * @author marlo
 */
public class VetorOrdenado {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner sc = new Scanner(System.in);
        int tamanhoVetor = 5;
        Integer[] vetor = new Integer[tamanhoVetor];
        Integer[] vetorOrdenado = new Integer[tamanhoVetor];
        
        try{
        // Entrada de dados
        for(int i = 0; i < tamanhoVetor; i++){
            System.out.println("Informe o valor do vetor na posição " + i);
            vetor[i] = sc.nextInt();
        }
        
        int indMenor = 0;
        int menor = Integer.MAX_VALUE;

        // Processamento
        for(int i = 0; i < tamanhoVetor; i++){ //  

            for(int v = 0; v < tamanhoVetor; v++ ){ //
                
                if (vetor[v] != null){
                    
                    if(vetor[v] < menor){
                        menor = vetor[v];
                        indMenor = v;
                    }
                    
                }
                
            }
            
            vetorOrdenado[i] = menor;
            menor = Integer.MAX_VALUE;
            vetor[indMenor] = null;
            
        }
        System.out.println();
        System.out.println("Resultado Ordenado:");
        // Saida de dados
        for(int valor : vetorOrdenado){
            System.out.println(valor);
        }
        
        // Tratamento de excessões         
        }catch(Exception ex){
            System.out.println();
            System.out.println("Trola não carai, só números inteiros mané!");
        }
    }
    
}
