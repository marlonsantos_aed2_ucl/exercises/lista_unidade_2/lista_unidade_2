/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algoritmosatualizacao;

/**
 *
 * @author marlo
 */
public class AlgoritmosAtualizacao {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int tamanhoVetor = 50;
        int[] vetor = new int[tamanhoVetor];
        
        int valorVetor = 3 * tamanhoVetor;
        
        // PREENCHENDO O VETOR
        for(int i=0; i<tamanhoVetor; i++){
            vetor[i] = valorVetor;
            valorVetor -= 3;
        }
        
        // PESQUISA SEQUENCIAL E ATUALIZAÇÃO DO VALOR ENCONTRADO NA PESQUISA
        int chave = 36; // valor a ser pesquisado
        int reg = 100;  // valor a ser atribuido a chave encontrada 
        for(int i = 0; i < tamanhoVetor; i++){
            if(vetor[i] == chave){
                System.out.println("Chave encontrada na posição: " + i);
                System.out.println("Valor da chave: " + vetor[i]);
                
                vetor[i] = reg;
                
                System.out.println("Valor atualizado: " + vetor[i]);
            }
        }
        
        System.out.println("");
        System.out.println("Vetor não ordenado: ");
        for(int valor : vetor){
            System.out.println(valor);
        }
        
        // Ordenando o vetor
        for(int i = 0; i < tamanhoVetor; i++){ 

            for(int v = 0; v < tamanhoVetor; v++ ){ 
                
                if( v > 0 && vetor[v] < vetor[v-1]){
                    int aux = vetor[v-1];
                    vetor[v-1] = vetor[v];
                    vetor[v] = aux;
                }
                
            }
        }
        
        System.out.println("");
        System.out.println("Vetor ordenado: ");
        for(int valor : vetor){
            System.out.println(valor);
        }
        
        System.out.println("");
        
        // PESQUISA BINÁRIA E ATUALIZAÇÃO DO VALOR ENCONTRADO NA PESQUISA
        chave = 100;
        reg = 150;
        
        int inicio = 0;
        int ultimaPos = tamanhoVetor - 1;
        int metade = ultimaPos / 2;
        
        while (inicio <= ultimaPos) {
            if (vetor[metade] == chave) {
               
               System.out.println("Chave encontrada na posição: " + metade);
               System.out.println("Valor da chave: " + vetor[metade]);
               
               vetor[metade] = reg;
               
                System.out.println("Valor atualizado da chave: " + vetor[metade]);
               
            } else {
                if (chave < vetor[metade]) {
                    ultimaPos = metade - 1;
                    //System.out.println("metade -1");
                } else {
                    inicio = metade + 1;
                    //System.out.println("metade +1");
                }
            }
            
            metade = (inicio + ultimaPos) / 2;
            //System.out.println("metade = " + metade);
        }
        
    }
}
