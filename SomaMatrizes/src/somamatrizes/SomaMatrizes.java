/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package somamatrizes;

import java.util.Scanner;

/**
 *
 * @author marlo
 */
public class SomaMatrizes {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner sc = new Scanner(System.in);
        int linhasMatriz;
        int colunasMatriz;
        
        // Recebe a qtd de linhas e colunas que as duas matrizes terão
        System.out.println("Informe a qtd de linhas das matrizes: ");
        linhasMatriz = sc.nextInt();
        
        System.out.println("Informe a qtd de colunas das matrizes: ");
        colunasMatriz = sc.nextInt();
        
        // Instância as matrizes
        Integer matriz1[][] = new Integer[linhasMatriz][colunasMatriz]; 
        Integer matriz2[][] = new Integer[linhasMatriz][colunasMatriz]; 
        Integer matrizesSomadas[][] = new Integer[linhasMatriz][colunasMatriz]; 
        
        // Preenchendo valores da matriz 1
        for(int i = 0; i < linhasMatriz; i++){
            System.out.println("\nlinha "+String.valueOf(i+1)+" da matriz 1");
            for(int j = 0; j < colunasMatriz; j++){
                System.out.print("Coluna " + String.valueOf(j+1) + " da Linha " + String.valueOf(i+1) + ": ");
                matriz1[i][j] = sc.nextInt();
            }
        }
        
        System.out.println("");
        
        // Preenchendo valores da matriz 2
        for(int i = 0; i < linhasMatriz; i++){
            System.out.println("\nlinha "+String.valueOf(i+1)+" da matriz 2");
            for(int j = 0; j < colunasMatriz; j++){
                System.out.print("Coluna " + String.valueOf(j+1) + " da Linha " + String.valueOf(i+1) + ": ");
                matriz2[i][j] = sc.nextInt();
            }
        }
        
        System.out.println("");
        
        // Soma das Matrizes
        for(int i = 0; i < linhasMatriz; i++){
            for(int j = 0; j < colunasMatriz; j++){
                matrizesSomadas[i][j] = matriz1[i][j] + matriz2[i][j]; 
                System.out.println("Soma da coluna " + String.valueOf(j+1) + " da Linha " + String.valueOf(i+1) + " das duas matrizes: " + matrizesSomadas[i][j]);
            }
        }
        
        
    }
    
}
